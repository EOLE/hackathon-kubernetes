kubectl label nodes k8s01 node-role.kubernetes.io/master=""
kubectl label nodes k8s02 node-role.kubernetes.io/worker=""
kubectl label nodes k8s03 node-role.kubernetes.io/worker=""
kubectl label nodes k8s04 node-role.kubernetes.io/worker=""
kubectl label nodes k8s05 node-role.kubernetes.io/worker=""
kubectl label nodes k8s06 node-role.kubernetes.io/worker=""
kubectl label nodes k8sg1 node-role.kubernetes.io/storage=""
kubectl label nodes k8sg2 node-role.kubernetes.io/storage=""
kubectl label nodes k8sg3 node-role.kubernetes.io/storage=""

kubectl label nodes k8s01 app=executor
kubectl label nodes k8s02 app=executor
kubectl label nodes k8s03 app=executor
kubectl label nodes k8s04 app=executor
kubectl label nodes k8s05 app=executor
kubectl label nodes k8s06 app=executor

kubectl label nodes k8s01 k8s-app=kubernetes-dashboard
kubectl label nodes k8s02 k8s-app=kubernetes-dashboard
kubectl label nodes k8s03 k8s-app=kubernetes-dashboard
kubectl label nodes k8s04 k8s-app=kubernetes-dashboard
kubectl label nodes k8s05 k8s-app=kubernetes-dashboard
kubectl label nodes k8s06 k8s-app=kubernetes-dashboard

kubectl label nodes k8sg1 app=store 
kubectl label nodes k8sg1 storagenode=glusterfs

kubectl label nodes k8sg2 app=store 
kubectl label nodes k8sg2 storagenode=glusterfs

kubectl label nodes k8sg3 app=store 
kubectl label nodes k8sg3 storagenode=glusterfs
