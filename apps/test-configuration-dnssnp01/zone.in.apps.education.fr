; /var/named/zone.in.apps.education.fr
$TTL 86400
$ORIGIN in.apps.education.fr.
@ IN SOA dnssnp01.in.apps.education.fr. ns.in.apps.education.fr. (
   2019070801   ; sn
        10800   ; refresh (3 heures)
          600   ; retry (10 minutes)
      1814400   ; expiry (3 semaines)
        10800 ) ; minimum (3 heures)
@ IN          NS      dnssnp01.in.apps.education.fr.
esxsnp01    IN  A   172.29.32.1
esxsnp02    IN  A   172.29.32.2
esxsnp03    IN  A   172.29.32.3
vcentersnp01    IN  A   172.29.32.11
dnssnp01    IN  A   172.29.32.17
dnssnp02    IN  A   172.29.32.18
proxy01     IN  A   172.29.32.20
serverone    IN A   172.29.32.65
k8s01        IN A   172.29.32.70
k8s02        IN A   172.29.32.71
k8s03        IN A   172.29.32.72
k8s04        IN A   172.29.32.73
k8s05        IN A   172.29.32.74
k8s06        IN A   172.29.32.75
k8sG1        IN A   172.29.32.80
k8sG2        IN A   172.29.32.81
k8sG3        IN A   172.29.32.82

web.in.apps.education.fr.   IN  CNAME  k8s02
admin.in.apps.education.fr.   IN    CNAME  k8s01
dashboard.in.apps.education.fr.   IN    CNAME  k8s01
etna.in.apps.education.fr.   IN CNAME  k8s01