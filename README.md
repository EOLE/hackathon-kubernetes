# hackathon-kubernetes

## Kubernetes 

Ce projet se base sur l'installation d'un paquet Snap microk8s
```bash
$ snap install microk8s --classic
$ snap install microk8s
$ microk8s.kubectl get all --all-namespaces
$ microk8s.kubectl get no
$ microk8s.enable dns dashboard
$ microk8s.kubectl get all --all-namespaces
$ watch microk8s.kubectl get all --all-namespaces
```

### Instanciation d'une Apps Kubernetes

L'instanciation d'une Apps Kubernetes se fait via un fichier YAML définissant les différents besoins (Deployements, Services, Volumes, VolumeClaims, etc)
```bash
$ microk8s.kubectl apply -f kubernetes/ethercalc/ethercalc-deployment.yml

> deployment.extensions/ethercalc created
> service/ethercalc unchanged
> persistentvolume/ethercalc unchanged
> persistentvolumeclaim/ethercalc unchanged

```

### Services disponibles
Chaque service permet d'exposer les Pods d'un Deployment dans son prore Cluster ou vers l'extérieur

```bash
$ microk8s.kubectl get service --all-namespaces

NAMESPACE     NAME                   TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)             AGE
default       e-user                 LoadBalancer   10.152.183.199   <pending>     80:32204/TCP        16m
default       ethercalc              LoadBalancer   10.152.183.85    <pending>     80:31916/TCP        17h
default       filepizza              NodePort       10.152.183.62    <none>        80:32130/TCP        17h
default       kubernetes             ClusterIP      10.152.183.1     <none>        443/TCP             41h
default       mongodb                NodePort       10.152.183.47    <none>        8081:31405/TCP      16h
default       redis                  ClusterIP      10.152.183.189   <none>        6379/TCP            18h
default       scrumblr               LoadBalancer   10.152.183.239   <pending>     80:30600/TCP        17h
kube-system   heapster               ClusterIP      10.152.183.91    <none>        80/TCP              41h
kube-system   kube-dns               ClusterIP      10.152.183.10    <none>        53/UDP,53/TCP       41h
kube-system   kubernetes-dashboard   ClusterIP      10.152.183.138   <none>        443/TCP             41h
kube-system   monitoring-grafana     ClusterIP      10.152.183.51    <none>        80/TCP              41h
kube-system   monitoring-influxdb    ClusterIP      10.152.183.155   <none>        8083/TCP,8086/TCP   41h
kube-system   tiller-deploy          ClusterIP      10.152.183.52    <none>        44134/TCP           41h

```
Chaque Service s'exposent alors dans sa zone (ClusterIP, NodePort, LoadBalancer) sur un port arbitraire, mais pouvant etre personnalisé

Les services deviennent alors accessibles depuis l'extérieur sur l'ip publique de la machine

La communication des services se fait via le DNS fournis par Kubernetes par défaut 
